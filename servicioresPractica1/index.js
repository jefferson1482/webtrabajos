const express = require("express");

const app = express();

const PORT = 3000;

//persistencia de datos
let arreglo = [] ;

//exponer carpeta con recursos estaticos
app.use('/public', express.static( __dirname + '/public'));
//siempre se utilizara express.json() por los recursos dinamicos
app.use( express.json() );

//  http://localhost:3000/



//  estudiantes
app.get('/', (req, res)=>{
    res.send(arreglo);
});
///get consulta el elemento
//// /individual:/:nombre

app.get('/:nombre', (req,res)=>{
    //desestructuracion
    const { nombre } = req.params;
    const respuesta = arreglo.filter(p=>{
        return p.nombre===nombre
    });

    if (respuesta.length > 0)
    {
        res.status(200).send({
            respuesta: respuesta[0]
        })
    }
    else
    {
        res.status(404).send({
            respuesta:"No se puede encontrar"
        })
    }

    console.log(respuesta)
    //otra forma
    //res.send( req.params.nombre);
});

///post se agrega el elemento

app.post('/', (req,res)=>{
    const {body} = req;
    arreglo.push(body);
    res.status(201).send(
        {
            mensaje:"Se almaceno correctamente",
            respuesta: body
        }
        );
});

///put y patch es la validacion

app.put('/', (req, res)=>{
    const { nombre, curso } = req.body;
    let elemento = arreglo.filter(p=> p.nombre === nombre)[0];
    elemento.curso = curso;
    res.status(200).send({
        respuesta: elemento
    })
});

//delete

app.delete('/', (req,res)=>{
    const { nombre } = req.params;
    arreglo = arreglo.filter(p=> p.nombre !== nombre)
    res.status(200).send({
        respuesta: "Elemento eliminado"
    })
});

app.listen( PORT , () =>{
    console.log(`El servidor esta corriendo en el puerto ${PORT}`);
});