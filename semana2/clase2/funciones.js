//funcion normal, regular
function saludar(nombre) 
{
    return `Hola ${nombre}`;
}

//console.log(saludar('Jefferson'));

//funcion anonima
const saludar2 = function(parametro)
{   
    return `Hola ${parametro}`;
}

//saludar2('Rivera');

//funcion flecha
const saludar3 = (parametro)=>{
    return `Hola ${parametro}`;
}

function llamarSaludo(fn,parametro)
{
    return (fn(parametro));
}

console.log(llamarSaludo(saludar, "Yoselyn"));