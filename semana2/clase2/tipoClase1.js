let edad=21;

const persona =
{
    nombre:'Jefferson',
    apellido:'Vera',
    edad,
    esEstudiante:true,
    geolocalizacion:{
        lat:214.123,
        lng:123.234,
    },

    getNombreCompleto(){
        return this.nombre+" "+this.apellido;
    }
}



/*
//Esto clonar los atributos 
const estudiante = { ...persona ,geo: 'ejemplo' };

estudiante.nombre = "Juan";

console.log(persona);
console.log(estudiante);
*/



function mostrarPersona({nombre,apellido,geolocalizacion: { lat, lng}}) 
{
    console.log(lat);
    console.log(lng);
};

mostrarPersona(persona)



/*
console.log(persona.nombre);
console.log(persona.apellido);
console.log(persona.edad);
console.log(persona.getNombreCompleto());
*/