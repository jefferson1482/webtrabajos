const mongoose = require("mongoose");
const { Schema } = mongoose;

const NoticiaSchema = new Schema(
{
    titulo: { type:String },
    enlace: { type:String }
},
//marca de tiempo se puede obiar esta parte 
{
    timestamps:{createdAt:true, updatedAt:true } 
});

module.exports = mongoose.model("Noticia", NoticiaSchema);
