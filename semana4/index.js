////paquetes de tercero
const mongoose = require("mongoose");
const axios = require("axios").default;
//cheerio es jQuery en el servidor 
const cheerio = require ("cheerio");
const cron =require("node-cron");


////Importaciones para poder trabajar con modelos de mongoose
const {MONGO_URI}= require('./config');
const {Noticia } = require('./models');

//console.log (MONGO_URI);
//const modelo = require('./models/noticias.model');
 mongoose.connect(MONGO_URI, {useNewUrlParser:true, useUnifiedTopology: true});
//por cada modelo se necesita 
cron.schedule("* * * * * ", 
//1. conectarnos a la pagna web de cnnn en español 
async ()=> {
    const html = await axios.get("https://cnnespanol.cnn.com/");
//2. cargar el html de la pagina para poder trabajar cn el 
    //console.log(html.data);
    //filtrar infromacio de esta data 
    const $ = cheerio.load(html.data);
    const titulos = $(".news__title");
    let arregloNoticias = []
    //varios elementos recorrer por un each
//3. filtrar el hrtml solo las noticias 

    titulos.each((index, element)=>{
        //console.log($(element).text());
        //acceder al elemento hijo 
        //console.log( $(element).children().attr("href"));
//4. guardar las noticias en base de datos 
        const ObjetoNoticia=
        {
            titulo:  $(element).text() ,
            enlace:  $(element).children().attr("href") 
        }
        arregloNoticias = [ ...arregloNoticias, ObjetoNoticia ];

    })
    Noticia.create(arregloNoticias);
//5.que esta situacion dse guarde cada tiempo definido por nosotros 
})