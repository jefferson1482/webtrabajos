//"var" no es recomendado utilizar mejor es una constante "const" o variable "let"

function operacion(n1,n2, operacion = "sumar") 
{
    switch (operacion) {
        case "sumar":
            return (n1+n2);
            break;
        case "restar":
            return (n1-n2);
            break;
        case "multiplicar":
            return (n1*n2);
            break;
        case "dividir":
            return (n1/n2);
            break;
        default:
            return 0;
    }
}

function saludar(nombre) 
{
    //coercion de datos
    return (`hola como esta ${nombre}`);
}


//esta es una forma de exportar la funcion
//aqui se utiliza la desestruturacion
module.exports = {
    funcion1: operacion,
    funcion2: saludar,
    variable:9
};