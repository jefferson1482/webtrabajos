# Pasos del taller 
1. Ubique el dockerfile en el directorio  raiz
2. Configure el archivo docker
3. Luego se crea el archivo docker-compose
4. Se configura el archivo anteriormente creado, con sus respectivas dependencias como momgoDB
5. Contruimos y levantamos los contenedores con el comando "docker-compose up --build"
6. Realizamos las petisiones al serviso RES de la API con Posmas

######################################################################################
# Ubicar pantallas Postman con las pruebas realizadas
1. POST de la introducida de una bebida
![Screenshot](./imagen/cap1.PNG)
2. GET de la bebida introducida
![Screenshot](./imagen/cap2.PNG)
3. Vista desde el navegador
![Screenshot](./imagen/cap3.PNG)
