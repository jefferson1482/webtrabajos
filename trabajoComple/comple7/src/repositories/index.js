module.exports = {
    UserRepository: require ('./user.repository'),
    BebidaRepository: require ('./bebida.repository'),
    LocalidadRepository: require ('./localidad.repository'),
    ComboRepository: require ('./combo.repository')
}