const { Bebida } = require('../models');

function getBebidaPrecio() {
    return new Promise((resolve, reject) => {
        Bebida.find({}, function (err, bebidas) {
            if (err) return handleError(err);
            let bebida = '';
            bebidas.forEach(e => {
                bebida += `${e.nombre}: $${e.precio}` + "\n";
            });
            resolve(bebida);
        })
    })
}


module.exports = {
    getBebidaPrecio
}