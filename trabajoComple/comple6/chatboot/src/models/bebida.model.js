const mongoose = require('mongoose');
const { Schema } = mongoose;

const BebidaSchema = new Schema(
    {
        nombre: { type: String },
        cantidad: { type: String },
        precio: { type: String },
    },
    {
        timestamps: { createdAt: true, updatedAt: true }
    })

module.exports = mongoose.model("Bebida", BebidaSchema);
