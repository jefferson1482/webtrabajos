'use strict'

const mongoose = require('mongoose');
const { Schema } = mongoose;

const LocalidadSchema = new Schema(
    {
        nombre: { type: String },
        numero: { type: String },
        estado: { type: String },
    },
    {
        timestamps: { createdAt: true, updatedAt: true }
    })

module.exports = mongoose.model("Localidad", LocalidadSchema);
