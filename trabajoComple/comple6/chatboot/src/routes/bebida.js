var express = require('express');
var BebidaController = require('../controllers/bebida.controller');
var BebidaRouter = express.Router();

BebidaRouter.get('/bebida', BebidaController.getBebidaPrecio);

module.exports = BebidaRouter;
