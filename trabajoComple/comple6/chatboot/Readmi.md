# Pasos del taller 
1. Ubique el dockerfile en el directorio  raiz
2. Configure el archivo docker
3. construimos la imagen docker  con el comando: docker build . -t taller-6
4. Comprobe que se halla creado la imagen en el docker desktop
5. Ejecute  el docker con el comando docker run --name taller-6 -d chatbot-taller6
6. Borrar el docker
