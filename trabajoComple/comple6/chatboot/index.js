const mongoose = require('mongoose')
const express = require('express');
const app = require('./app');

const fs = require('fs');
const ora = require('ora');

//declarando variables
let bebida = "";
var contador = 0;

//variables de session
let client;
let sessionData;

const PORT = 4000;

// libreria para sms dinamicos
const chalk = require('chalk');
const qrcode = require('qrcode-terminal');
const { Client, MessageMedia } = require('whatsapp-web.js');
app.use(express.urlencoded({ extended: true }))
const SESSION_FILE_PATH = './session.json';

// librerias
const config = require('./src/config/index');

// TODO: Conection database - ya realizada
mongoose.connect(config.MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then(() => {
    console.log('MongoDB Connected…')
    app.listen(config.PORT, () => {
        console.log(`API REST corriendo: ${config.PORT}`)
    });
}).catch(err => console.log(err));

//creando variables de busqueda en la base de datos
async function cargarVariables() {
    bebida = await require('./src/controllers/bebida.controller').getBebidaPrecio();
    return bebida;
}

//ejecutando variables de busqueda
cargarVariables();

const sendMedia = (number, fileName) => {
    number = number.replace('@c.us', '');
    number = `${number}@c.us`
    const media = MessageMedia.fromFilePath(`./mediaSend/${fileName}`);
    client.sendMessage(number, media);
}


// enviar sms
const sendMessage = (number = null, text = null) => {
    number = number.replace('@c.us', '');
    number = `${number}@c.us`
    const message = text;
    client.sendMessage(number, message);
    console.log(`${chalk.red('⚡⚡⚡ Enviando mensajes....')}`);
}


// ESCUCHAR SMS
const listenMessage = () => {
    client.on('message', async msg => {
        const { from, to, body } = msg;
        // console.log(msg.hasMedia);
        if (msg.hasMedia) {
            const media = await msg.downloadMedia().then(msg.getMentions());
        }
        // console.log(body);
        await replyAsk(from, body);
    });
}


// client.on('message', async (msg) => {
//     const mentions = await msg.getMentions();
    
//     for(let contact of mentions) {
//         console.log(`${contact.pushname} was mentioned`);
//     }
// });


//RESPONDER SMS
const replyAsk = (from, answer) => new Promise((resolve, reject) => {
    console.log(`Sms recibido:`, answer);

    let smsRecibido = answer.toLowerCase();

    if ("c1" == smsRecibido || "c2" == smsRecibido || "c3" == smsRecibido || "f1" == smsRecibido || "f2" == smsRecibido) {
        sendMessage(from,
            (`Reservación realizada 🎉
        F - Finalizar reservación
        0 - Ir al menú`));
    } else if (contador < 1) {
        sendMessage(from,
        (`👋 Hola, Bienvenid@ a Ova Club!
        ¿Sobre qué deseas saber?
        1 - Ver bebidas disponibles
        2 - Ver localidades
        G - Despedirse`)
        );
        contador += 1;
    } else {
        switch (smsRecibido) {
            case "0":
                sendMessage(from,
                (`¡OVA CLUB!
                1 - Ver bebidas disponibles
                2 - Ver Localidades
                G - Despedirse`));
                break;
            case "1":
                sendMessage(from,
                (`Estas son las bebidas disponibles:

                ${bebida}

                0 - Ir al menú
                2 - ¿Deseas ver las localidades?
                G - Despedirse`));
                break;
            case "2":
                sendMessage(from,
                (`¿Sobre que localidad deseas saber?
                CV - Localidad Vip
                CF - localidad Free
                0  - Menú principal`));
                break;
            case "cv":
                sendMedia(from, 'cVip.jpeg');
                break;
            case "cf":
                sendMedia(from, 'zFree.jpg');
                break;
            case "f":
                sendMessage(from, 'Gracias, en breve un agente 🗣️ se contactara para finalizar la transacción');
                break;
            case "g":
                sendMessage(from, 'Que tengas un exelente día 🎉');
                break;
            default:
                sendMessage(from,
                    (`Ups, caracter incorrecto❗`)
                )
                break;
        }
    }
})


// Revisamos si tenemos credenciales guardadas para inciar sessio
const withSession = () => {
    // Si exsite cargamos el archivo con las credenciales
    const spinner = ora(`Cargando ${chalk.yellow('Validando sessión...')}`);
    sessionData = require(SESSION_FILE_PATH);
    spinner.start();
    client = new Client({
        session: sessionData
    });

    client.on('ready', () => {
        console.log('listo!');
        spinner.stop();
        connectionReady();
    });

    client.on('auth_failure', () => {
        spinner.stop();
        console.log('Error de autentificación, generar el QRCODE');
    })
    client.initialize();
}


// Generamos un QRCODE para iniciar sesion
const withOutSession = () => {
    console.log('No tenemos session guardada');
    client = new Client();
    client.on('qr', qr => {
        qrcode.generate(qr, { small: true });
    });

    client.on('ready', () => {
        console.log('Client is ready!');
        connectionReady();
    });

    client.on('auth_failure', () => {
        console.log('* Error de autentificacion vuelve a generar el QRCODE *');
    })

    client.on('authenticated', (session) => {
        // Guardamos credenciales
        sessionData = session;
        fs.writeFile(SESSION_FILE_PATH, JSON.stringify(session), function (err) {
            if (err) {
                console.log(err);
            }
        });
    });
    client.initialize();
}

const connectionReady = () => {
    listenMessage();
}

(fs.existsSync(SESSION_FILE_PATH)) ? withSession() : withOutSession();

app.listen( PORT , () =>{
    console.log(`El servidor esta corriendo en el puerto ${PORT}`);
});