const express = require('express');
const app = express();

//midelwares
app.use(express.json());

//cargar rutas
let bebida = require('./src/routes/bebida');

//definicion de rutas
app.use('/api', bebida);

module.exports = app;