//Estos 3 arreglos deben tener relación con el proceso principal de su proyecto. Por ejemplo, 
//si fuese consultar los cursos que hay online: Nombre materia, horario y total de estudiantes.

const materias = [
    {
        id: 1,
        titulo: "Programacion Orientado a Objetos",
        idnombre: 2,
        idmaestro: 3
    },
    {
        id: 2,
        titulo: "Redes",
        idnombre: 3,
        idmaestro: 1
    },
    {
        id: 3,
        titulo: "Enrutamiento",
        idnombre: 1,
        idmaestro: 2
    }
]

const estudiantes = [
    {
        id: 1,
        nombre: "Vera Jefferson"
    },
    {
        id: 2,
        nombre: "Vera Joselin"
    },
    {
        id: 3,
        nombre: "Rivas Erick"
    }
]


const docentes = [
    {
        id: 1,
        maestro: "Pico Josselyn"
    },
    {
        id: 2,
        maestro: "Rivera Sara"
    },
    {
        id: 3,
        maestro: "Zambrano Jose"
    }
]

//consultar el curso por id 

async function consultarmateria(id) {
        const materia = materias.find((materia) => materia.id === id);
        if (!materia) {
            const error = new Error();
            error.message = "Materia no encontrada ";
            throw error;
        }
        return materia;
}

//consultar el estudiante

async function consultarestudiante(id) {
        const estudiante = estudiantes.find((estudiante) => estudiante.id === id);
        if (!estudiante) {
            const error = new Error();
            error.message = "Estudiante no encontrado ";
            throw error;
        }
        return estudiante;
}

//consultar el docente

async function consultardocente(id) {
        const docente = docentes.find((docente) => docente.id === id);
        if (!docente) {
            const error = new Error();
            error.message = "Total estudiantes no encontrado ";
            throw error;
        }
        return docente;
}

// mostrar el resultado 
async function main (){
    try{
        const materia = await consultarmateria(1);
        const estudiante = await consultarestudiante(materia.idnombre);
        const docente = await consultardocente(materia.idmaestro);
        console.log(`El curso es ${materia.titulo}, su estudiante es: ${estudiante.nombre} 
            y su docente es ${docente.maestro}`);
    }catch(error){
        console.log(error.message);
    }
}

main();
