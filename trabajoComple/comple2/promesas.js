//Estos 3 arreglos deben tener relación con el proceso principal de su proyecto. Por ejemplo, 
//si fuese consultar los cursos que hay online: Nombre materia, horario y total de estudiantes.


const materias = [
    {
        id: 1,
        titulo: "Programacion Orientado a Objetos",
        idnombre: 2,
        idmaestro: 3
    },
    {
        id: 2,
        titulo: "Redes",
        idnombre: 3,
        idmaestro: 1
    },
    {
        id: 3,
        titulo: "Enrutamiento",
        idnombre: 1,
        idmaestro: 2
    }
]

const estudiantes = [
    {
        id: 1,
        nombre: "Vera Jefferson"
    },
    {
        id: 2,
        nombre: "Vera Joselin"
    },
    {
        id: 3,
        nombre: "Rivas Erick"
    }
]


const docentes = [
    {
        id: 1,
        maestro: "Pico Josselyn"
    },
    {
        id: 2,
        maestro: "Rivera Sara"
    },
    {
        id: 3,
        maestro: "Zambrano Jose"
    }
]

function consultarmateria(id) {
    return new Promise((resolve, reject) => {
        const materia = materias.find((materia) => materia.id === id);
        if (!materia) {
            const error = new Error();
            error.message = "Materia no encontrada ";
            reject(error);
        }
        resolve(materia);
    })
}

//consultar el estudiante

function consultarestudiante(id) {
    return new Promise((resolve, reject) => {
        const estudiante = estudiantes.find((estudiante) => estudiante.id === id);
        if (!estudiante) {
            const error = new Error();
            error.message = "Estudiante no encontrado ";
            reject(error);
        }
        resolve(estudiante);
    })
}

//consultar el docente
function consultardocente(id) {
    return new Promise((resolve, reject) => {
        const docente = docentes.find((docente) => docente.id === id);
        if (!docente) {
            const error = new Error();
            error.message = "Docente no encontrado ";
            reject(error);
        }
        resolve(docente);
    })
}

//Consultar tota de estudiantes por materia 

let materiaAuxiliar = [];
let materia2Auxiliar = [];

consultarmateria(2).then((materia) => {
    materiaAuxiliar = materia;
    return consultarestudiante(materia.idnombre);
})

    .then((estudiante) => {
        materiaAuxiliar.estudiante = estudiante;
        delete materiaAuxiliar.idnombre;
    })
    

consultarmateria(2).then((materia) => {
    materia2Auxiliar = materia;
    return consultardocente(materia.idmaestro);
})
    .then((docente) => {
       materia2Auxiliar.docente = docente;
       delete materia2Auxiliar.idmaestro;
       console.log(materia2Auxiliar)
    })

    .catch((err) => {
        console.log(err.message);
    })

    











