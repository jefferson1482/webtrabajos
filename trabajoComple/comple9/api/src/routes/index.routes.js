module.exports = {
    HomeRoutes: require('./home.routes'),
    UserRoutes: require('./user.routes'),
    BebidaRoutes: require('./bebida.routes'),
    LocalidadRoutes: require('./localidad.routes'),
    ComboRoutes: require('./combo.routes')
}