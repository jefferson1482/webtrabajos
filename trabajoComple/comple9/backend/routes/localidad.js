var express = require('express');
var router = express.Router();
const {LocalidadController}  = require('../controllers/index');

router.get('/', LocalidadController.getLocalidad);
router.post('/', LocalidadController.postLocalidad);
router.get('/:id', LocalidadController.updateLocalidadForm);
router.put('/:id', LocalidadController.updateLocalidad);
router.delete('/:id', LocalidadController.deleteLocalidad);

module.exports = router;
