var express = require('express');
var router = express.Router();
const ComboController = require('../controllers/combo');

router.get('/', ComboController.getCombo);
router.post('/', ComboController.postCombo);
router.get('/:id', ComboController.updateComboForm);
router.put('/:id', ComboController.updateCombo);
router.delete('/:id', ComboController.deleteCombo);

module.exports = router;
