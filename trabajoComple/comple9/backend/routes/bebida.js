var express = require('express');
var router = express.Router();

const { BebidaController } = require('../controllers/index');

router.get('/', BebidaController.getBebidas);
router.post('/', BebidaController.postBebida);
router.get('/:id', BebidaController.updateBebidaForm);
router.put('/:id', BebidaController.updateBebida);
router.delete('/:id', BebidaController.deleteBebida);

module.exports = router;
