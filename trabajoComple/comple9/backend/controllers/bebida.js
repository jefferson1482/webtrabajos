var axios = require("axios").default;
const url = "http://localhost:5000/v1/api/bebida/";

async function getBebidas(req, res, next) {
    const bebida = await axios.get(url);
    res.render('bebida', { bebidas: bebida });
}

async function postBebida(req, res, next) {
    const { nombre, precio, cantidad } = req.body;
    await axios.post(url, { nombre, precio, cantidad });
    res.redirect('/bebida');
}

async function updateBebidaForm(req, res, next) {
    const data = await axios.get(url + req.params.id);
    res.render('partials/form-edit', { data: data.data });
}

async function updateBebida(req, res, next) {
    const { nombre, precio, cantidad } = req.body;
    await axios.patch(url + req.params.id, { nombre, precio, cantidad });
    res.redirect('/bebida');
}

async function deleteBebida(req, res, next) {
    await axios.delete(url + req.params.id);
    res.redirect('/bebida');
}

module.exports = {
    getBebidas,
    postBebida,
    updateBebidaForm,
    updateBebida,
    deleteBebida
}