var axios = require("axios").default;
const url = "http://localhost:5000/v1/api/localidad/";

async function getLocalidad(req, res, next) {
    const localidad = await axios.get(url);
    res.render('localidad', { localidades: localidad });
}

async function postLocalidad(req, res, next) {
    const { nombre, precio, numero, descripcion } = req.body;
    await axios.post(url, { nombre, precio, numero, descripcion });
    res.redirect('/localidad');
}

async function updateLocalidadForm(req, res, next) {
    const localidad = await axios.get(url + req.params.id);
    res.render('partials/form-edit-localidad', { data: localidad.data });
}

async function updateLocalidad(req, res, next) {
    const { nombre, precio, numero, descripcion } = req.body;
    await axios.patch(url + req.params.id, { nombre, precio, numero, descripcion });
    res.redirect('/localidad');
}

async function deleteLocalidad(req, res, next) {
    await axios.delete(url + req.params.id);
    res.redirect('/localidad');
}

module.exports = {
    getLocalidad,
    postLocalidad,
    updateLocalidadForm,
    updateLocalidad,
    deleteLocalidad
}