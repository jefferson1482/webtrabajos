var axios = require("axios").default;
const url = "http://localhost:5000/v1/api/combo/";

async function getCombo(req, res, next) {
    const combo = await axios.get(url);
    res.render('combo', { combos: combo });
}

async function postCombo(req, res, next) {
    const { nombre, zona, precio, descripcion } = req.body;
    await axios.post(url, { nombre, zona, precio, descripcion });
    res.redirect('/combo');
}

async function updateComboForm(req, res, next) {
    const data = await axios.get(url + req.params.id);
    res.render('partials/form-edit-combo', { data: data.data });
}

async function updateCombo(req, res, next) {
    const { nombre, zona, precio, descripcion } = req.body;
    await axios.patch(url + req.params.id, { nombre, zona, precio, descripcion });
    res.redirect('/combo');
}

async function deleteCombo(req, res, next) {
    await axios.delete(url + req.params.id);
    res.redirect('/combo');
}

module.exports = {
    getCombo,
    postCombo,
    updateComboForm,
    updateCombo,
    deleteCombo
}