const { Router } = require('express');
// const { UserController } = require('../controllers')

module.exports = function ({ UserController }) {
    const router = Router();
    router.get('/:bebidaId', UserController.get);
    router.get('', UserController.getAll);
    router.post('/', UserController.create);
    router.patch('/:bebidaId', UserController.update);
    router.delete('/:bebidaId', UserController.delete);

    return router;
}
