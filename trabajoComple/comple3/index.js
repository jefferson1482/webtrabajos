
const mongoose = require("mongoose");
const axios = require("axios").default;
const cheerio = require ("cheerio");
const cron =require("node-cron");


////Importaciones para poder trabajar con modelos de mongoose
const {MONGO_URI}= require('./config');
const {Noticia } = require('./models');


 mongoose.connect(MONGO_URI, {useNewUrlParser:true, useUnifiedTopology: true});
cron.schedule("40 8 * * * ", 

async ()=> {
    const html = await axios.get("https://www.extra.ec/temas/cronica-roja");
    const $ = cheerio.load(html.data);
    const titulos = $(".title");
    let arregloNoticias = []

    titulos.each((index, element)=>{
        const ObjetoNoticia=
        {
            titulo:  $(element).text() ,
            enlace:  $(element).children().attr("href") 
        }
        arregloNoticias = [ ...arregloNoticias, ObjetoNoticia ];

    })
    Noticia.create(arregloNoticias);
})