const express = require('express');
const app= express();
const mongoose = require("mongoose");


const { MONGO_URI } = require("./config");
const { Noticia } = require("./models");
// midelware 
app.use(express.json());

//conexion a la base de datos
mongoose.connect(MONGO_URI, { useNewUrlParser:true, useUnifiedTopology:true } );

// peticion get que devuelve la base de datos 
app.get('/',  (req,res) => {
        Noticia.find({},(err,docs)=>{        
        res.send({
            docs
        })
        
    });
    
})

app.listen(5000, ()=>{
    console.log(`El servidor esta ejecutandose en el puerto 5000`)
})

//trabajoComple3