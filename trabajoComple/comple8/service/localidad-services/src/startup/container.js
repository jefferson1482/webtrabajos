const { createContainer, asClass, asValue, asFunction } = require('awilix');

const config = require('../config');
const app = require('./index');

// importar servicios
const {LocalidadService} = require('../services');

// importar controladores
const { LocalidadController } = require('../controllers');

// importar rutas
const Routes = require('../routes')
const { LocalidadRoutes} = require('../routes/index.routes');

// models
const { LocalidadModel} = require('../models');

// repositorios
const {LocalidadRepository} = require('../repositories');


const container = createContainer();
container
    .register(
        {
            app: asClass(app),
            router: asFunction(Routes).singleton(),
            config: asValue(config)
        }
    )
    .register(
        {
            LocalidadService: asClass(LocalidadService).singleton()
        }
    ).register(
        {
            LocalidadController: asClass(LocalidadController.bind(LocalidadController)).singleton()
        }
    ).register(
        {
            LocalidadRoutes: asFunction(LocalidadRoutes).singleton()
        }
    ).register(
        {
           
            Localidad: asValue(LocalidadModel)
        }
    ).register(
        {
            LocalidadRepository: asClass(LocalidadRepository).singleton()
        }
    )


module.exports = container;