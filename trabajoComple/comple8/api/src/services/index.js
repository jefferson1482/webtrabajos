module.exports = {
    HomeService: require('./home.service'),
    UserService: require('./user.service'),
    BebidaService: require('./bebida.service'),
    ComboService: require('./combo.service')
}