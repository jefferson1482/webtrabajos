const { createContainer, asClass, asValue, asFunction } = require('awilix');

const config = require('../config');
const app = require('./index');

// importar servicios
const { HomeService, UserService, BebidaService, ComboService } = require('../services');

// importar controladores
const { HomeController, UserController, BebidaController,ComboController } = require('../controllers');

// importar rutas
const Routes = require('../routes')
const { HomeRoutes, UserRoutes, BebidaRoutes,ComboRoutes } = require('../routes/index.routes');

// models
const { UserModel, BebidaModel,ComboModel} = require('../models');

// repositorios
const { UserRepository, BebidaRepository,ComboRepository } = require('../repositories');


const container = createContainer();
container
    .register(
        {
            app: asClass(app),
            router: asFunction(Routes).singleton(),
            config: asValue(config)
        }
    )
    .register(
        {
            HomeService: asClass(HomeService).singleton(),
            UserService: asClass(UserService).singleton(),
            BebidaService: asClass(BebidaService).singleton(),
            ComboService: asClass(ComboService).singleton()
        }
    ).register(
        {
            HomeController: asClass(HomeController.bind(HomeController)).singleton(),
            UserController: asClass(UserController.bind(UserController)).singleton(),
            BebidaController: asClass(BebidaController.bind(BebidaController)).singleton(),
            ComboController: asClass(ComboController.bind(ComboController)).singleton()
        }
    ).register(
        {
            HomeRoutes: asFunction(HomeRoutes).singleton(),
            UserRoutes: asFunction(UserRoutes).singleton(),
            BebidaRoutes: asFunction(BebidaRoutes).singleton(),
            ComboRoutes: asFunction(ComboRoutes).singleton()
        }
    ).register(
        {
            User: asValue(UserModel),
            Bebida: asValue(BebidaModel),
            Combo: asValue(ComboModel)
        }
    ).register(
        {
            UserRepository: asClass(UserRepository).singleton(),
            BebidaRepository: asClass(BebidaRepository).singleton(),
            ComboRepository: asClass(ComboRepository).singleton()
        }
    )


module.exports = container;