const express= require("express");
const app =  express();

const response={
    data:[],
    services:"Users services",
    architecture:"Microservices"
}
const logger = message => console.log("Users Service:"+ message);

app.use((req,res,next)=>{
    response.data=[];
    next();
})

app.get("/api/v2/users", (req,res)=>{
    response.data.push("User12", "Admins", "Invitado");
    logger("Get User Data")
    return res.send(response);
})

module.exports = app;