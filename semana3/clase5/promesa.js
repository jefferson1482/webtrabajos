const libros = [
    {
        id:1,
        titulo:"JavaScritp en el Cliente ",
        idautor:3
    },
    {
        id:2,
        titulo:"JavaScritp en el Servidor",
        idautor:1
    },
    {
        id:3,
        titulo:"PHP MySql y Apche",
        idautor:2
    }
];

const autores = [
    {
        id:1,
        nombre:"Vera Jefferson",
    },
    {
        id:2,
        nombre:"Villamar Dayanna",
    },
    {
        id:3,
        nombre:"Josselyn",
    }
];

function buscarLibroId(id) 
{
    return new Promise((resolve, reject)=>
    {
        const libro = libros.find((libro)=> libro.id === id);
        if (!libro) 
        {
            const error = new Error();
            error.message = "Libro no encontrado";
            reject(error);
        }
        resolve(libro);
    })
};

function buscarAutorId(id) 
{
    return new Promise((resolve, reject)=>
    {
        const autor = autores.find((autor)=> autor.id === id);
        if (!autor) 
        {
            const error = new Error();
            error.message = "Autor no encontrado";
            reject(error);
        }
        resolve(autor);
    })
};

let libroAuxiliar={};

buscarLibroId(1).then((libro)=>
{
    libroAuxiliar = libro;
    return (buscarAutorId(libro.idautor))
})
.then((autor)=>
{
    libroAuxiliar.autor = autor;
    delete libroAuxiliar.idautor;
    console.log(libroAuxiliar);
})
.catch((err)=>
{
    console.log(err.message);
})
