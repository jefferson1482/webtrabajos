const libros = [
    {
        id:1,
        titulo:"JavaScritp en el Cliente ",
        idautor:3
    },
    {
        id:2,
        titulo:"JavaScritp en el Servidor",
        idautor:1
    },
    {
        id:3,
        titulo:"PHP MySql y Apche",
        idautor:2
    }
];

const autores = [
    {
        id:1,
        nombre:"Vera Jefferson",
    },
    {
        id:2,
        nombre:"Villamar Dayanna",
    },
    {
        id:3,
        nombre:"Josselyn",
    }
];

async function buscarLibroId(id) 
{

        const libro = libros.find((libro)=> libro.id === id);
        if (!libro) 
        {
            const error = new Error();
            error.message = "Libro no encontrado";
            throw error;
        }
        return libro;
};

async function buscarAutorId(id) 
{
        const autor = autores.find((autor)=> autor.id === id);
        if (!autor) 
        {
            const error = new Error();
            error.message = "Autor no encontrado";
            throw error;
        }
        return autor;
};

async function main() 
{
    try{
        const libro = await buscarLibroId(2);
        const autor = await buscarAutorId(libro.idautor);

        console.log(`El libro es ${libro.titulo} y su autor: ${autor.nombre}`)
    }catch (error) {
        console.log(error.message)
    }
}

main();