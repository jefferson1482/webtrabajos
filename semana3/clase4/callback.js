const libros = [
    {
        id:1,
        titulo:"JavaScritp en el Cliente ",
        idautor:3
    },
    {
        id:2,
        titulo:"JavaScritp en el Servidor",
        idautor:1
    },
    {
        id:3,
        titulo:"PHP MySql y Apche",
        idautor:2
    }
];

const autores = [
    {
        id:1,
        nombre:"Vera Jefferson",
    },
    {
        id:2,
        nombre:"Villamar Dayanna",
    },
    {
        id:3,
        nombre:"Josselyn",
    }
];

function buscarLibroPorId(id,callback)
{
    const libro= libros.find((libro)=> libro.id===id);
    if (!libro) {
         //generar una excencion
        const error = new Error();
        error.message = "Libro no encontrado";
        //devolver esa excepcion a traves del callback
        return callback(error);
    }
    //devolver el callback con el libro encontrada
    callback(null,libro);
}

function buscarAutorId(id,callback) 
{
    const autor = autores.find((autor)=> autor.id===id);
    if (!autor) 
    {
        const error = new Error();
        error.message="Autor no encontrado";
        return callback(error);
    }
    callback(null, autor);
}

buscarLibroPorId(1, (err, libro)=>
{
  if(err)
  {
      return console.log(err.message);
  }
  buscarAutorId(libro.idautor, (err, autor)=>
  {
      if (err) {
          return console.log(err.message);
      }
      libro.autor = autor;
      delete libro.idautor;
      console.log(libro);
  })
})